import numpy as np
import matplotlib.pyplot as plt
from pprint import pprint
import scipy.integrate as integrate


def new_sin(x):
	sin_values = {np.pi: 0, 2*np.pi: 0}
	return float(sin_values[x] if x in sin_values.keys() else np.sin(x))


def new_cos(x):
	cos_values = {np.pi/2: 0, 3*np.pi/2: 0}
	return float(cos_values[x] if x in cos_values.keys() else np.cos(x))


def func(x):
	return x - np.log1p(x*x)


def fl(a,b):		
	return float((b - a) / 2)


def fa(x, l, k):
	return 1 / l + integrate.quad(lambda x: func(x) * new_cos(np.pi * k * x / l), -1 * l, l)[0]

def fb(x, l, k):
	return 1 / l + integrate.quad(lambda x: func(x) * new_sin(np.pi * k * x / l), -1 * l, l)[0]


def new_func(x, l, p):
	res = [0, 0]
	y = 0
	pi = np.pi
	for i in range(1, p):
		res = fa(x[i], l, 0) / 2 + sum(fa(x[i], l, k) * new_cos(np.pi * k * x[i] / l) + fb(x[i], l, k) * new_sin(np.pi * k * x[i] / l) for k in range(0, n))

	return res

	# for k in range(0, p):
	# 	for n in range(0, l):
	# 		res[0] += 2 / l + 2 / l * func(n) * new_cos(2 * k * pi / l) 
	# 		res[1] += 2 / l + 2 / l * func(n) * new_sin(2 * k * pi / l)
	# 	y += res[0] * new_cos(2 * k * pi * x / l) + res[1] * new_sin(2 * k * pi * x / l)

	# return y


def fanull(l, k):
	a = 0
	for i in range(0, l):
		a += 2 / l * func(k)

	return a





a = 0
b = 4
n = 11

x = []
y = []

s = (b - a) / (n - 1)

for i in range(0, n):
	x.append(i * s)
	y.append(func(i * s))

# print("Table value function:")
# print(x)
# print(y)

N = [2, 4, 8, 16]

ynn = []

ynn.append(new_func(x, n, 16))

nx = np.arange(a, b, 0.01)
plt.plot(nx, func(nx), color='r')
# for i in range(0, len(N)):
	# yn = new_func(x, n, N[i])
	# plt.plot(x, yn, color='g')	
	# plt.grid(True)
plt.plot(x, ynn, color='g')	
plt.show()
print(ynn)










