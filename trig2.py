import numpy as np
import matplotlib.pyplot as plt

def new_sin(x):
	sin_values = {np.pi: 0, 2*np.pi: 0}
	return float(sin_values[x] if x in sin_values.keys() else np.sin(x))


def new_cos(x):
	cos_values = {np.pi/2: 0, 3*np.pi/2: 0}
	return float(cos_values[x] if x in cos_values.keys() else np.cos(x))


def func(x):
	return x - np.log1p(x*x)

def A(j, n):
	s = 0.0
	for i in range(0, n):
		s += func(2 * np.pi * i / (2 * n + 1)) * new_cos(2 * np.pi * j * i / (2 * n + 1));
	if j == 0:
		return 1 / (2 * n + 1 ) * s;

	return 2 / (2 * n + 1) * s;


def B(j, n):
	ii = 0
	s = 0.0
	for i in range(-n, n + 1):
		ii = -n + i;
		s += func(2 * np.pi * i / (2 * n + 1)) * new_sin(2 * np.pi * j * i / (2 * n + 1));
	return 2 / (2 * n + 1) * s


def interpolate(x, a, b, n):
	s = a[0]
	for i in range(1, n):
		s += a[i] * new_cos(i * x) + b[i] * new_sin(i * x)
	return s

size = 11
n = 11
		

a = []

for i in range(0, n):
	a.append(A(i, n))

b = []
b.append(0)

for i in range(1, n):
	b.append(B(i, n))	

x = []
y = []

for i in range(0, size):
	x.append(-np.pi + 2 * np.pi / size * i)
	y.append(interpolate(x[i], a, b, n))

plt.plot(a, b, color='g')	
plt.show()

	