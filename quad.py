import math
import numpy as np
import matplotlib.pyplot as plt
import scipy as sp

# Our function
def func(x):
    return x - np.log1p(1 + x*x)

# Get n points in [a, b]
def get_points(a, b, n):
    x = sp.linspace(a, b, n)
    y = func(x)
    return x, y

# Get matrix and solution for second degree polynomial
def mnk_2(x, y):
    sum_x = {}
    for pow in range(0, 5):
        sum_x[pow] = sum([math.pow(xi, pow) for xi in x])

    sum_xy = {}
    for pow in range(0, 3):
        sum_xy[pow] = sum([math.pow(xi, pow) * yi for xi, yi in zip(x,y)])

    A = [
        [11, sum_x[1], sum_x[2]],
        [sum_x[1], sum_x[2], sum_x[3]],
        [sum_x[2], sum_x[3], sum_x[4]]
    ]
    B = [
        sum_xy[0],
        sum_xy[1],
        sum_xy[2]
    ]

    solution = np.linalg.solve(A, B)[::-1]
    polynomial = sp.poly1d(solution)
    return polynomial

# Get matrix and solution for fourth degree polynomial
def mnk_4(x, y):
    sum_x = {}
    for pow in range(0, 9):
        sum_x[pow] = sum([math.pow(xi, pow) for xi in x])
    sum_xy = []
    for pow in range(0, 5):
        sum_xy.append(sum([math.pow(xi, pow) * yi for xi, yi in zip(x,y)]))
    A = [
        [sum_x[8], sum_x[7], sum_x[6], sum_x[5], sum_x[4]],
        [sum_x[7], sum_x[6], sum_x[5], sum_x[4], sum_x[3]],
        [sum_x[6], sum_x[5], sum_x[4], sum_x[3], sum_x[2]],
        [sum_x[5], sum_x[4], sum_x[3], sum_x[2], sum_x[1]],
        [sum_x[4], sum_x[3], sum_x[2], sum_x[1], 11],
    ]
    B = sum_xy[::-1]
    solution = np.linalg.solve(A, B)
    polynomial = sp.poly1d(solution)
    return polynomial

if __name__ == '__main__':
    a, b = 0.0, 4.0
    x, y = get_points(a, b, 11)

    polynomial_2 = mnk_2(x, y)
    polynomial_4 = mnk_4(x, y)
    print(polynomial_2)
    print(polynomial_4)

    nx = np.arange(a, b, 0.01)
    plt.grid(True)
    plt.plot(nx, func(nx), color='b')
    plt.plot(x, polynomial_2(x), color='g')
    plt.plot(x, polynomial_4(x), color='r')
    plt.show()
