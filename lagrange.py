from __future__ import division
import scipy
from math import log, cos, pi
import matplotlib.pyplot as plt
import numpy as np

def get_xs(start, stop, number):
    return [(stop - start) * i / (number - 1) for i in range(0, number)]

def chebyshev_xs(start, stop, number):
    return [(cos((2 * k + 1) / (2 * number) * pi) + 1) / 2 * (stop - start) + start for k in range(number)]

def get_points(f, start, stop, number, ch = False):
    if ch:
        xs = chebyshev_xs(start, stop, number)
    else:
        xs = get_xs(start, stop, number)
    ys = [f(x) for x in xs]
    return (xs, ys)

def print_table(data):
    print('\n'.join('{0:7.3f} |{1:7.3f}'.format(x, y) for x, y in zip(data[0], data[1])))

def print_points(points):
    print(' '.join('({0:6.3f}, {1:6.3f})'.format(x, y) for x, y in zip(points[0], points[1])))

def lagrange(x, y):
    result = scipy.poly1d([0.0]) #setting result = 0
    for i in range(0,len(x)): #number of polynomials L_k(x).
        temp_numerator = scipy.poly1d([1.0]) # resets temp_numerator such that a new numerator can be created for each i.
        denumerator = 1.0 #resets denumerator such that a new denumerator can be created for each i.
        for j in range(0,len(x)):
            if i != j:
                temp_numerator *= scipy.poly1d([1.0,-x[j]]) #finds numerator for L_i
                denumerator *= x[i]-x[j] #finds denumerator for L_i
        result += (temp_numerator/denumerator) * y[i] #linear combination
    return result

def plot(x, func):
    x_val = np.arange(min(x), max(x) + 1, 0.1) # generates x values we would like to evaluate.
    plt.xlabel('x')
    plt.ylabel('f(x)')
    plt.grid(True)
    plt.plot(x_val, func(x_val)) # gives the value of our Lagrange polynomial.

def plot_table(x, y):
    plt.xlabel('x'); plt.ylabel('y')
    plt.grid(True)
    plt.plot(x, y) #result(x_val) gives the value of our Lagrange polynomial.
    plt.axis([min(x)-1, max(x)+1, min(y)-1, max(y)+1])

def calculate(f, points):
    polynomial = lagrange(points[0], points[1])
    return polynomial

def get_error(p, f, xs):
    diff = [abs(f(x) - p(x)) for x in xs]
    return max(diff)

def make(f, points, start, stop):
    p = calculate(f, points)
    print_points(points)
    print("Polynomial: {}".format(p))
    print("\nPolynomial values:")
    p_points = get_points(p, start, stop, 11)
    print_table(p_points)
    print("\nFunction values:")
    f_points = get_points(f, start, stop, 11)
    print_table(f_points)
    error = get_error(p, f, p_points[0])
    print("Error is {}".format(error))
    plot(f_points[0], p)
    # plot(f_points[0], f)
    return p


if __name__ == "__main__":
    f = lambda x: x - np.log(1 + x*x)

    points_3 = get_points(f, 0, 4, 3)
    print("\n\n~~~~~~~~~~~~ 3 points ~~~~~~~~~~~~")
    p_3 = make(f, points_3, 0, 4)

    points_5 = get_points(f, 0, 4, 5)
    print("\n\n~~~~~~~~~~~~ 5 points ~~~~~~~~~~~~")
    p_5 = make(f, points_5, 0, 4)

    points_ch = get_points(f, 0, 4, 5, ch = True)
    print("\n\n~~~~~~~~~~~~ ch points ~~~~~~~~~~~~")
    p_ch = make(f, points_ch, 0, 4)

    xs = get_xs(0, 4, 100)
    plt.plot(xs, [f(x) for x in xs], color='black')
    plt.show()

    plot(xs, lambda x: abs(f(x) - p_3(x)))
    plot(xs, lambda x: abs(f(x) - p_5(x)))
    plot(xs, lambda x: abs(f(x) - p_ch(x)))
    plt.show()
