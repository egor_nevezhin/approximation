import matplotlib.pyplot as plt
from numpy.polynomial.legendre import Legendre
import numpy as np
import scipy as sp

def func(x):
    return x - np.log1p(1 + x*x)


mn = 0.0
mx = 4.0
n = 11

l = mx - mn


x = sp.linspace(mn, mx, n)
y = func(x)



# res = numpy.polynomial.legendre.legfit(x, y, 4)
# f = sp.poly1d(res)

# print (f)

print (Legendre(2))

# nx = numpy.arange(mn, mx, 0.01)
# plt.plot(nx, func(nx), color='r')
# plt.plot(x, f(x), color='g')
# plt.xlabel(r'$x$') 
# plt.ylabel(r'$f(x)$') 
# plt.title(r'$f_1(x)=x - ln(1-x^2), f_2(x) - approximate$')
# plt.grid(True)
# plt.show()    